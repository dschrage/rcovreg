functions {
  // Helper function for LKJ transform.
  // This computes the sum over j_prime < j from 35.11 of Stan 2.17.0 ref.
  real sum_square_x(matrix x, int i, int j) {
    int j_prime;
    real sum_x = 0;
    // See note 3: a sum with no terms is defined as 0.
    if(j==1) return(sum_x);

    j_prime = 1;
    while(j_prime < j) {
      sum_x += x[i,j_prime]^2;
      j_prime += 1;
    }
    return(sum_x);
  }
  // Given a vector of reals that have already been constrained to (-1,1)
  // using tanh(), use the LKJ transform to create a lower-triangular matrix
  // that's the Cholesky factor of a correlation matrix.
  // Notation follows the Stan manual (section 35.11 of version 2.17.0).
  matrix lkj_to_chol_corr(row_vector constrained_reals, int n_outcomes) {
    int z_counter;
    // X is the cholesky factor of the correlation matrix.
    matrix[n_outcomes,n_outcomes] x;

    // Instead of constructing the lower-triangular z matrix, use the z
    // values by implicitly addressing into the constrained reals.
    z_counter = 1;
    // Initialize the first row: first element is 1; all else zero.
    x[1,1] = 1;
    for(j in 2:n_outcomes) {
      x[1,j] = 0;
    }
    // Fill the diagonal and lower triangle of x.
    for(i in 2:n_outcomes) {
      for(j in 1:n_outcomes) {
        if(i==j) {
          // x[i,j] = sqrt(1 - (sum(x[i,1:j]^2)));
          x[i,j] = sqrt(1 - sum_square_x(x, i, j));
        } else if(i > j) {
          // x[i,j] = constrained_reals[z_counter]*sqrt(1 - (sum(x[i,1:j]^2)));
          x[i,j] = constrained_reals[z_counter]*sqrt(1 - sum_square_x(x, i, j));
          z_counter += 1;
        } else { // above diagonal is all zeros
          x[i,j] = 0;
        }
      }
    }
    return(x);
  }

  // Compute the likelihood in pieces for within-chain parallelization.
  real partial_sum_lpdf(row_vector[] y_slice, int start, int end,
    matrix x_beta, matrix x_delta, matrix x_gamma,
    matrix beta, matrix gamma, matrix delta,
    int n_outcomes, int delta_size) {

    // number of observations in this slice.
    int n_obs = end - start + 1;

    matrix[n_obs,n_outcomes] beta_x_product;
    matrix[n_obs,delta_size] delta_x_product;
    matrix[n_obs,n_outcomes] gamma_x_product;

    delta_x_product = tanh(x_delta[start:end,] * delta);
    gamma_x_product = sqrt(exp(x_gamma[start:end,] * gamma));
    beta_x_product = x_beta[start:end,] * beta;

    real partial_sum = 0.0; // running sum of likelihood in this slice
    for(i in 1:n_obs) {
      partial_sum += multi_normal_cholesky_lupdf(y_slice[i] |
                       beta_x_product[i,],
                       diag_pre_multiply(gamma_x_product[i,],
                        lkj_to_chol_corr(delta_x_product[i,], n_outcomes)));
    }
    return(partial_sum);
  }
}
data {
  int<lower=1> n_obs; // # of obs
  int<lower=1> n_predictors_beta; // # of params (including intercept)
  int<lower=1> n_predictors_gamma; // # of params (including intercept)
  int<lower=1> n_predictors_delta; // # of params (including intercept)
  int<lower=2> n_outcomes; // p
  int<lower=1> grainsize; // parameter for slicing the data across threads

  row_vector[n_outcomes] y[n_obs];
  matrix[n_obs,n_predictors_beta] x_beta; // mean predictors
  matrix[n_obs,n_predictors_gamma] x_gamma; // variance predictors
  matrix[n_obs,n_predictors_delta] x_delta; // correlation predictors
}
transformed data {
  int delta_size;
  matrix[n_obs,n_predictors_beta] x_beta_Q;
  matrix[n_predictors_beta,n_predictors_beta] x_beta_R;
  matrix[n_predictors_beta,n_predictors_beta] x_beta_R_inv;
  matrix[n_obs,n_predictors_gamma] x_gamma_Q;
  matrix[n_predictors_gamma,n_predictors_gamma] x_gamma_R;
  matrix[n_predictors_gamma,n_predictors_gamma] x_gamma_R_inv;
  matrix[n_obs,n_predictors_delta] x_delta_Q;
  matrix[n_predictors_delta,n_predictors_delta] x_delta_R;
  matrix[n_predictors_delta,n_predictors_delta] x_delta_R_inv;

  delta_size = (n_outcomes * (n_outcomes-1))/2;

  // Thin QR decomposition of X data.
  x_beta_Q = qr_thin_Q(x_beta) * sqrt(n_obs - 1);
  x_beta_R = qr_thin_R(x_beta) / sqrt(n_obs - 1);
  x_beta_R_inv = inverse(x_beta_R);
  x_gamma_Q = qr_thin_Q(x_gamma) * sqrt(n_obs - 1);
  x_gamma_R = qr_thin_R(x_gamma) / sqrt(n_obs - 1);
  x_gamma_R_inv = inverse(x_gamma_R);
  x_delta_Q = qr_thin_Q(x_delta) * sqrt(n_obs - 1);
  x_delta_R = qr_thin_R(x_delta) / sqrt(n_obs - 1);
  x_delta_R_inv = inverse(x_delta_R);
}
parameters {
  matrix[n_predictors_beta,n_outcomes] beta_tilde; // mean coefficients
  matrix[n_predictors_gamma,n_outcomes] gamma_tilde; // variance coefficients
  // There's a delta for each unique combination of outcomes: n*(n-1) / 2
  matrix[n_predictors_delta,delta_size] delta_tilde; // rho coefficients
}
model {
  // weak priors on model coefficients for a tiny amount of regularization.
  to_vector(beta_tilde) ~ cauchy(0, 1);
  to_vector(gamma_tilde) ~ cauchy(0, 1);
  to_vector(delta_tilde) ~ cauchy(0, 1);

  target += reduce_sum_static(partial_sum_lupdf, y, grainsize,
      x_beta_Q, x_delta_Q, x_gamma_Q,
      beta_tilde, gamma_tilde, delta_tilde,
      n_outcomes, delta_size);
}
generated quantities {
  // Invert the QR decomposition so parameters are on the original data scale.
  matrix[n_predictors_beta,n_outcomes] beta; // mean coefficients
  matrix[n_predictors_gamma,n_outcomes] gamma; // variance coefficients
  // There's a delta for each unique combination of outcomes: n*(n-1) / 2
  matrix[n_predictors_delta,delta_size] delta; // rho coefficients

  for(i in 1:n_outcomes) {
    beta[,i] = x_beta_R_inv * beta_tilde[,i];
    gamma[,i] = x_gamma_R_inv * gamma_tilde[,i];
  }
  for(i in 1:delta_size) {
    delta[,i] = x_delta_R_inv * delta_tilde[,i];
  }
}
