# Covariance Regression using R

This repository provides R software to implement the covariance regression model described in our paper:

* **Bloome, Deirdre and Daniel Schrage. 2019.** "[Covariance Regression Models for Studying Treatment Effect Heterogeneity Across One or More Outcomes: Understanding How Treatments Shape Inequality.](https://doi.org/10.1177/0049124119882449)" *Sociological Methods and Research*.

We fit the model using Markov Chain Monte Carlo (MCMC) as implemented in [Stan](https://mc-stan.org/).

We plan to release this as a full R package in the future; for now, we simply provide a number of R functions to make it easy to fit the covariance regression model to data and to use the estimates to make inferences.

This vignette provides an overview of the model, describes the steps needed to install the software to fit the model, shows how to simulate fake data from the model, demonstrates how to fit the model to data, and finally shows how to calculate quantities of interest for inference.

## Our Covariance Regression Model

In the paper above, we introduce a covariance regression model for modeling multiple outcomes. Here we provide a very brief overview of the model specification.

```math
\begin{align}
\bold{Y_i} &\sim \text{Multivariate~Normal}(\bold{\mu_i}, \bold{\Sigma_i})\\
\bold{\mu_i} &= \bold{X_i\beta}\\
\bold{\Sigma_i} &= \bold{S_i} \bold{R_i} \bold{S_i}\\
\log \sigma^2_{i} &= \bold{X_i\gamma}\\
c_{ij} &= \tanh(\bold{X_i} \delta_j)
\end{align}
```

Here, $`\bold{Y_i}`$ is a $`1 \times p`$ matrix of outcome data. $`\bold{\mu_i}`$ is a vector of means for each outcome, and our model parameterizes the means conditional on a matrix of predictors $`\bold{X}`$ and regression coefficients $`\beta`$, which are the same as what you would see in standard OLS regression. Covariance regression, however, also models the covariance matrix $`\bold{\Sigma_i}`$ of $`\bold{Y_i}`$, separating it into a diagonal matrix $`\bold{S_i}`$ of standard deviations and the correlation matrix $`\bold{R_i}`$. We model the log of the squared standard deviations $`\sigma^2_{i}`$ of the matrix $`\bold{S_i}`$ with a matrix of predictors $`\bold{X}`$ and regression coefficients $`\gamma`$. And we model the canonical partial correlations $`c_{ij}`$ of the correlation matrix $`\bold{R_i}`$ with a matrix of predictors $`\bold{X}`$ and parameters $`\delta`$. We describe these canonical partial correlations (CPCs) and the LKJ transformation from which they come in [more detail below](#lkj-parameterization-of-the-correlation-matrix). For $`p`$ outcomes, there are $`\frac{p(p-1)}{2}`$ CPCs that define the correlation matrix, and that also defines the number of $`\delta`$ parameters in the model.

Note that while $`\bold{X}`$ is shared across the mean, standard deviation, and correlation model in the specification above, this is only for simplicity of presentation. The model as implemented in the software here can define different sets of predictors for each of the three models, as shown below.


#### Differences from the published model
We have made one change and one addition to the covariance regression model implemented here that distinguishes it from the model published in the paper above, though the substantive uses and reasons for fitting the model are identical:

##### LKJ parameterization of the correlation matrix
First, we modified the parameterization of the correlation matrix in order to make it more computationally stable for data with more than 3 outcomes. Even with 3 outcomes, the proportion of all matrices that are positive semidefinite (and thus valid correlation matrices) is fairly small and falls off rapidly above 3 outcomes. This makes it difficult for MCMC to explore the space of correlation matrices in models with many outcomes.

To fix this, we shifted from directly parameterizing the correlations to instead use the [LKJ transformation](https://mc-stan.org/docs/2_26/reference-manual/correlation-matrix-transform-section.html#correlation-matrix-inverse-transform) that transforms a correlation matrix into a set of canonical partial correlations (CPCs), and we then parameterize the CPCs. The CPC parameters are not directly interpretable on the correlation scale (though the *signs* of the parameters are still interpretable as increasing or decreasing correlations). To properly interpret the magnitude of the $`\delta`$, you should generate predicted values of the correlations and use them to create marginal effects or other quantities of interest.

Using the LKJ parameterization not only solves the computational problems that develop with 3 or more outcomes; it also substantially speeds up the model fit.

##### QR decomposition of data before fitting
Second, to speed up model fitting and avoid convergence issues when predictors and their associated parameters are on very different scales, we first apply the [QR decomposition](https://mc-stan.org/users/documentation/case-studies/qr_regression.html) to transform the data. Under the hood, this creates modified parameters `beta_tilde`, `gamma_tilde`, and `delta_tilde` that provide estimates for the transformed data. We then invert the decomposition and return the parameters `beta`, `gamma`, and `delta` on the scale of the original data, as they are described above. Those latter 3 are the parameter output you should care about, and you should ignore the `_tilde` parameter estimates.


## Installation of `cmdstanr`
You may first need to update your R installation. We have tested on R back to version 3.6.3 and on newer 4.x versions. (The R version is printed in the R console when it first starts up.) If any package installation fails and you're using a version of R older than 3.6.3, try [updating R](https://cloud.r-project.org/).

We use the [`cmdstanr`](https://mc-stan.org/cmdstanr/) package to run the [Stan](https://mc-stan.org/) program that implements our covariance regression model. The most up-to-date installation instructions can be found on the [`cmdstanr` website](https://mc-stan.org/cmdstanr/), but you should be able to install the latest version by running the following in a fresh R session:
```r
install.packages("cmdstanr", repos = c("https://mc-stan.org/r-packages/", getOption("repos")))
```

The first time you run Stan, you need to install CmdStan. To do that, you'll need a working C++ compiler. You can check to see if you already have the necessary requirements using this:
```r
cmdstanr::check_cmdstan_toolchain()
```

If you need to install C++ tools, you can find [full instructions on the CmdStan website](https://mc-stan.org/docs/2_26/cmdstan-guide/cmdstan-installation.html) for [Linux](https://mc-stan.org/docs/2_26/cmdstan-guide/cmdstan-installation.html#linux-g-and-make), [MacOS](https://mc-stan.org/docs/2_26/cmdstan-guide/cmdstan-installation.html#macos-clang-and-make), and [Windows](https://mc-stan.org/docs/2_26/cmdstan-guide/cmdstan-installation.html#windows-make).

Once you have the required C++ toolchain, you can use `cmdstanr` to install Stan:
```r
library(cmdstanr)
install_cmdstan(cores = 2)
```
This will take a few minutes, but you only have to do this when you first install `cmdstanr`. It will install Stan to your local user directory, so if you're working in a shared computing environment, it should work fine without any special permissions.


<!-- Add section explaining why we don't use rstan? The short answer is that CRAN only has rstan 2.21, and we need Stan 2.25 in order to do our parallel/multithreaded version, and we couldn't get the development version of rstan to work. -->

#### Install Other Required R Packages
* `rstan`
By default, model results will be returned in `rstan` format (i.e., as a `stanfit` object). To use this functionality, you'll need to install the `rstan` package (`install.packages("rstan")`).

* `nimble`
The simulation of fake data below relies on the `nimble` package to generate draws from a multivariate normal distribution using a Cholesky factor of the covariance matrix. It's not a terribly common package, so you may have to install it (`install.packages("nimble")`).

This will choose a nearby CRAN repository for faster downloads and install both packages:
```r
local({r <- getOption("repos")
       r["CRAN"] <- "https://cloud.r-project.org"
       options(repos=r)
})
install.packages("rstan")
install.packages("nimble")
```


## Load the `rcovreg.R` Code
You should download the files `rcovreg.R` and `covreg_lkj_parallel_qr.stan` from this repository (see the file list above) and put them in your working directory (you can use `getwd()` to see the current working directory and `setwd()` to change the working directory).

```r
source("rcovreg.R")
```

The very first time you run this, this will take up to a minute to compile the model. (You'll see a message "Compiling Stan program...") After that, though, it should load instantly, because it will cache the compiled model.

This will also load `cmdstanr`, `nimble`, and `rstan` packages, along with all the helper functions needed to simulate fake data, fit the covariance regression model, and examine the results.

Mac users: We have seen compilation freeze at this stage when using the R Console in the R.app GUI on MacOS, despite a working C++ toolchain and Stan installation. If you encounter this problem, you can instead open a terminal and run R (by typing `R`) or use RStudio to avoid this.

## Simulating Fake Data from the Model

We first show how to simulate fake data from our covariance regression. This allows you to verify that the software is valid: it should recover the true model parameters from which the data were simulated. But it also works as a good example dataset to demonstrate how to fit the model.

In this example, we will simulate data for 3 outcomes, with $`k=2`$ predictors plus an intercept used to model the means, the standard deviations, and the correlations. We first need to choose parameter values for each of the key parameters $`\beta`$, $`\gamma`$, and $`\delta`$ described in the model above. Here, `beta` and `gamma` are each $`3 \times 3`$ matrices: each row corresponds to a single outcome (`y1`, `y2`, and `y3`) and each column corresponds to a predictor (the intercept, `x1`, and `x2`).  `delta` is also a $`3 \times 3`$ matrix: As with `beta` and `gamma`, each column corresponds to a predictor. The rows correspond to each unique correlation, and 3 outcomes require 3 unique correlations. In general, $`p`$ outcomes imply $`\frac{p(p-1)}{2}`$ unique correlations (e.g., 4 outcomes would require a 6-row delta matrix for its 6 unique correlations).

```r
# Parameters for the mean model.
beta <- t(matrix(c(
  #Int    x1      x2
  0.5,     1,     -1,  # y1
  1.5,   1.5,    0.5,  # y2
 -0.5,  -0.5,    1.5)  # y3
  , nrow=3, ncol=3, byrow=TRUE
  ))
# Parameters for the model of standard deviations.
gamma <- t(matrix(c(
  #Int    x1      x2
 0.25,  -0.1,    0.1,  # y1
 0.15, -0.15,    0.2,  # y2
-0.25, -0.15,   0.25)  # y3
  , nrow=3, ncol=3, byrow=TRUE
  ))

# Parameters for the correlation model.
delta <- t(matrix(c(
  #Int    x1      x2
 0.35,  -0.1,   -0.2,  # y2,y1
 0.15, -0.15,    0.2,  # y3,y1
-0.25,  0.25,  -0.25)  # y3,y2
  , nrow=3, ncol=3, byrow=TRUE
  ))

```

Using these parameters, we can simulate data from the covariance regression model. Here, we will simulate $`n=2000`$ observations from the model with the parameters above. (The $`\bold{X}`$ data are just independent normally distributed data, since the goal here is simply to demonstrate how to fit the model and that the model will recover the true parameter values.)

```r
fake_data <- sim_fake_covreg_data(beta, gamma, delta, n=2000, k=2, n_outcomes=3)
head(fake_data)
```




## Fitting the Model to Data

First, put your data in a format that can be used by Stan using the `make_covreg_data()` function. This function takes the following arguments:
* `y`: matrix or data.frame of multiple outcomes.
* `beta_predictors`: matrix or data.frame of predictors for the mean model.
* `gamma_predictors`: matrix or data.frame of predictors for the standard deviation model. If no data are provided, it automatically uses the same predictors as in `beta_predictors`.
* `delta_predictors`: matrix or data.frame of predictors for the correlation model. If no data are provided, it automatically uses the same predictors as in `beta_predictors`.
* To fit only an intercept for any (or all) of these models, pass `NA` as the argument.

```r
covreg_data <- make_covreg_data(y=fake_data[,c("y1", "y2", "y3")],
  beta_predictors=fake_data[,c("x1", "x2")],
  gamma_predictors=fake_data[,c("x1", "x2")],
  delta_predictors=fake_data[,c("x1", "x2")])
```

You can use `make_covreg_data()` on your own datasets, too, and it will put everything in the proper order expected by the Stan model. (Your variable names do not have to follow this `y1, y2...` and `x1, x2,...` convention; these are just examples.) Note that Stan does not handle missing data, so you should pass complete data to this function. Otherwise, fitting the model below will fail with an error about missing data.

Second, fit the model to the data. The first parameter to the `fit_covreg` function is the data; the other parameters are described below in the sections on [computation](#speeding-computation-using-multiple-cores-in-parallel) and [convergence](#checking-convergence).
```r
covreg_output <- fit_covreg(covreg_data,
  chains=4, parallel_chains=2, threads_per_chain=2)
```

This will take a bit of time, and it will print an update every 500 iterations (or more or less often if you set the `refresh` parameter to a different value). On a fast 16-core machine (AMD 2950X), 4 chains, 4 in parallel, and 4 threads per chain, this takes about 50 seconds to run. On a slow 2-core machine from 2012 (Intel i3-3225), 4 chains, 2 in parallel, and 1 thread per chain takes about 8 minutes to run.


Finally, you can print the output of the key parameters. By default, output is returned in `rstan` format as a `stanfit` object. Alternatively, the [`posterior` package](https://mc-stan.org/posterior/) provides a nice set of tools for working with `cmdstanr` output, and you can get output in `cmdstanr` format by setting `rstan_output=FALSE` in `fit_covreg()`. This demo will show the default`rstan` output only.

```r
print(covreg_output, pars=c("beta", "gamma", "delta"))
beta
gamma
delta
```

You can compare the parameter estimates from the model output to the true parameter values used to simulate the data. Because of sampling error, they won't match exactly, but they should be close. If you want a closer match, you can increase `n` above when you simulate the data. (If you go up to `n=20000` you should see all parameters within about 0.01 of their true value, but it will take 5x-10x as much time to run as it did with 2000 observations.) Note that the variance and correlation models will require a larger number of observations to get rid of sampling variation than the mean model will, so the estimates of `beta` are more likely to be close to the truth in small samples than are `gamma` and `delta`. But with a large enough sample, they will all converge to the true parameter values.


#### Speeding Computation: Using Multiple Cores in Parallel
Our Stan covariance regression model is programmed to take advantage of multiple processing cores on your computer to speed up the model fit. *We recommend using only as many threads as you have physical cores.* Many modern processors support simultaneous multithreading (SMD; or what Intel calls "HyperThreading") that allow multiple threads to run on a single physical processing core. This works great for most day-to-day computing, but it's not so great for statistical computation.

To find out how many physical cores you have, you can run `parallel::detectCores(logical=FALSE)` in R on MacOS and Windows. On Linux, you'll need to open a terminal and run `cat /proc/cpuinfo | grep 'core id'`, then multiply the number of cores per socket by the number of sockets.

`fit_covreg()` takes a few different parameters that describe how to make use of multiple processing cores. The `chains` parameter indicates how many separate MCMC chains to run. We recommend 4 chains (and never less than 3). The `parallel_chains` parameter specifies how many chains to fit simultaneously. The `threads_per_chain` parameter specifies how many cores to use in parallel for computing each MCMC chain. You'll get more of a speed gain by increasing the number of parallel chains (up to the total number of chains), and only then should you start to use multiple threads per chain. Here are some rules of thumb that are probably ideal for every number of cores except 6; with 6 cores, you might want to run only 3 chains and increase the number of sampling iterations (the `iter_sampling` parameter, which defaults to 500; with 3 chains, use more like 700).

| Number of physical cores | `parallel_chains` | `threads_per_chain` |
| ------------------------ | ----------------  | --------------------|
| 2                        | 2                 | 1                   |
| 4                        | 4                 | 1                   |
| 6                        | 3                 | 2                   |
| 8                        | 4                 | 2                   |
| 12                       | 4                 | 3                   |
| 16                       | 4                 | 4                   |

Even if you have a larger server with 32 or 64 or more cores, there's not much benefit of going higher than 4 threads per chain, thanks to [Amdahl's law](https://en.wikipedia.org/wiki/Amdahl's_law), at least based on our tests (but if you have a large dataset, you might experiment on your own). So the settings for 16 cores are likely also the most appropriate for bigger machines, too (and you can instead use your extra cores to fit multiple covariance regression models simultaneously, since no one ever fits just one model!).

`fit_covreg()` also takes a `grainsize` parameter, which determines how to split the data across parallel threads when using multiple cores. The default value of 1 chooses a grainsize automatically, though it may not be optimal. The Stan User's Guide describes [how to choose a grainsize manually](https://mc-stan.org/docs/2_26/stan-users-guide/reduce-sum.html#reduce-sum-grainsize). We have found that a bad grainsize can slow the model down by a factor of 2; the default grainsize should hopefully not fall in the "bad" range. If you're fitting the model to very large datasets where fits take hours, it's perhaps worth reading the Stan manual on this and testing different grainsizes to find the best one. Otherwise, you're probably fine with the default value. This only affects the speed of the fit, not any substantive aspects of the fit.



## Checking Convergence
As part of the model fit, the model will attempt to automatically check for convergence problems using the `cmdstan_diagnose()` function from `cmdstanr` (for details, see the [`CmdStan` manual entry here](https://mc-stan.org/docs/2_26/cmdstan-guide/diagnose.html)). One primary thing this does is to examine the $`\hat{R}`$ values for signs of failure to converge. The values of $`\hat{R}`$ are also printed as part of the `print()` function above when viewing model output. One rule of thumb is that $`\hat{R}`$ [should be below 1.05](https://mc-stan.org/rstan/reference/Rhat.html) for all parameters. If not, you can try increasing the number of warmup iterations (parameter `iter_warmup` in `fit_covreg()`)

You can use `rstan` to look at traceplots and examine convergence on your own, e.g.,
```r
traceplot(covreg_output, pars=c("beta"))
```


## Calculating Predicted Values
To do inference directly on the parameters, you can use the `print()` function as shown above, which provides point estimates (mean) and intervals (e.g., use the 2.5% and 97.5% values to construct a 95% uncertainty interval) for each parameter.

Far more useful, though, is to take the parameters and use them to calculate quantities of interest such as marginal effects or predicted values. You can use the following code to get the posterior parameter draws for every parameter in the model (assuming you stuck with the default `rstan_output=TRUE`):

```r
draws <- extract(covreg_output, pars=c("beta", "gamma", "delta"))
```

This returns a list of 3 arrays, one each for `beta`, `gamma`, and `delta`. For example, with the simulated data above, `beta` contains 2000 draws of a $`3\times3`$ matrix of parameters. You can see the first draw with the code `draws$beta[1,,]`.

You can use all these parameter draws to compute any quantity of interest you want and then use them to make inferences. Here, for example, you could multiply each $`3\times3`$ array in `beta` with a matrix of $`\bold{X}`$ data and get 2000 expected values for each outcome. The mean of those 2000 values is the point estimate of the expected value, and you can construct a 95% uncertainty interval by simply taking the 2.5% and 97.5% quantile (i.e., `quantile(<expected_values>, probs=c(0.025, 0.975))`, where `<expected_values>` is the variable containing your expected values).


#### Helper Functions for Calculating Predicted Values
COMING SOON! Anything you need to do can be done with the posterior draws above, but we're working on some more generic code to do this for you.



## Citation
If you find this software useful, you can cite the article on which it is based:
```
  @article{BloomeSchrage19,
    title={Covariance Regression Models for Studying Treatment Effect Heterogeneity Across One or More Outcomes: Understanding How Treatments Shape Inequality},
    author={Bloome, Deirdre and Schrage, Daniel},
    journal={Sociological Methods and Research},
    year={2019}
  }
```


